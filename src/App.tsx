import React, { useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { ApolloProvider } from "@apollo/client";
import client from "./graphql/client";
import ProductList from "./components/ProductList";
import ProductDetail from "./components/ProductDetail";
import FilterSidebar from "./components/FilterSidebar";

import "./App.css";

const App: React.FC = () => {
  

  return (
    <ApolloProvider client={client}>
      <Router>
        <div className="mt-4 d-flex">
          <div className="flex-grow-1">
            <Routes>
              <Route
                path="/"
                element={<ProductList />}
              />
              <Route path="/product/:id" element={<ProductDetail />} />
            </Routes>
          </div>
        </div>
      </Router>
    </ApolloProvider>
  );
};

export default App;
