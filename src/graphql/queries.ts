import { gql } from "@apollo/client";

export const GET_PRODUCTS = gql`
  query GetProducts($category: String, $page: Int, $itemsPerPage: Int) {
    products(category: $category, page: $page, itemsPerPage: $itemsPerPage) {
      edges {
        id
        name
        price
        category
        image
      }
      pageInfo {
        currentPage
        totalPages
      }
    }
  }
`;

export const GET_PRODUCT = gql`
  query GetProduct($id: ID!) {
    product(id: $id) {
      id
      name
      price
      category
      image
      description
    }
  }
`;

export const GET_CATEGORIES = gql`
  query GetCategories {
    categories {
      name
    }
  }
`;
