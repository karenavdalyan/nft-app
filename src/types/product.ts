export interface Product {
  id: string;
  name: string;
  price: number;
  category: string;
  image: string;
  description?: string;
}

export interface ProductsData {
  products: {
    edges: Product[];
    pageInfo: {
      currentPage: number;
      totalPages: number;
    };
  };
}

export interface ProductData {
  product: Product;
}
