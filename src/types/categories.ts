export interface Category {
  name: string;
}

export interface CategoriesData {
  categories: Category[];
}
