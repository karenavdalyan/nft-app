import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useQuery } from "@apollo/client";
import { GET_PRODUCTS } from "../graphql/queries";
import { ProductsData } from "../types/product";
import FilterSidebar from "./FilterSidebar";

const ProductList: React.FC = () => {
  const [page, setPage] = useState<number>(1);
  const [selectedCategory, setSelectedCategory] = useState<string>("");

  const { loading, error, data } = useQuery<ProductsData>(GET_PRODUCTS, {
    variables: { category: selectedCategory, page, itemsPerPage: 5 },
  });

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error.message}</p>;

  if (!data || !data.products) {
    return <p>Products not found.</p>;
  }

  const { edges, pageInfo } = data.products;

  const handlePageChange = (newPage: number) => {
    setPage(newPage);
  };

  const handleCategoryChange = (category: string) => {
    setSelectedCategory(category);
  };

  return (
    <div className="d-flex">
      <div className="col-md-3 mx-2">
        <FilterSidebar
          selectedCategory={selectedCategory}
          onCategoryChange={handleCategoryChange}
        />
      </div>
      <div className="d-flex flex-column align-items-center flex-grow-1">
        <div className="d-flex gap-3 mb-4 flex-wrap">
          {edges.map((product) => (
            <div key={product.id} className="card p-1 nft-card">
              <img
                src={product.image}
                alt={product.name}
                className="card-img-top"
              />
              <h5 className="card-title">{product.name}</h5>
              <p className="card-text">${product.price.toFixed(2)}</p>
              <Link to={`/product/${product.id}`} className="btn btn-primary">
                View Details
              </Link>
            </div>
          ))}
        </div>

        {/* Pagination controls */}
        <div className="pagination">
          {Array.from({ length: pageInfo.totalPages }, (_, index) => (
            <button key={index} onClick={() => handlePageChange(index + 1)}>
              {index + 1}
            </button>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ProductList;
