import React from "react";
import { useParams } from "react-router-dom";
import { useQuery } from "@apollo/client";
import { GET_PRODUCT } from "../graphql/queries";
import { ProductData } from "../types/product";

const ProductDetail: React.FC = () => {
  const { id } = useParams<{ id: string }>();

  const { loading, error, data } = useQuery<ProductData>(GET_PRODUCT, {
    variables: { id },
  });

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error.message}</p>;

  if (!data || !data.product) {
    return <p>Product not found.</p>;
  }

  const product = data.product;

  return (
    <div className="d-flex gap-3 flex-wrap mx-3">
      <img className="img-fluid rounded nft-details-image" src={product.image} alt={product.name} />
      <div className="card-body">
        <h1 className="card-title">{product.name}</h1>
        <p className="card-text">Price: ${product.price.toFixed(2)}</p>
        <p className="card-text">Category: {product.category}</p>
        <p className="card-text">Description: {product.description}</p>
      </div>
    </div>
  );
};

export default ProductDetail;
