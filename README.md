# NFT Marketplace

This repository contains the front-end components of the NFT Marketplace application. The application allows users to view and filter a list of NFTs fetched from a GraphQL API. The front-end is built using React and TypeScript.

### Prerequisites

- Node.js
- npm (Node Package Manager)

### Installation

1. Clone the repository:

   `git clone https://gitlab.com/karenavdalyan/nft-app`

2. Change to the front-end directory:

   `cd nft-app`

3. Install dependencies:

   `npm install`

### Configuration

Before running the front-end, configure the GraphQL API endpoint in the `.env` file. Create a `.env` file in the root directory of the front-end and add:

   ```
   REACT_APP_GRAPHQL_API_URL=<your-graphql-api-url>
   ```

   Replace `<your-graphql-api-url>` with the actual GraphQL API endpoint for the backend.

### Running the Application

Start the front-end development server:

   `npm start`

The application will be available at [http://localhost:3000](http://localhost:3000).

### Deployment

Build the production-ready front-end application:

   `npm run build`

The built files will be available in the `build` directory.
